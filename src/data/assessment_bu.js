,
		{
			"title": "Assessment",
			"lessons":[
				{
					"title": "Assessment Lesson",
					"sections": [
						{
							"Banner": {
								"title": "Assessment",
								"subTitle": "Sample Assessment",
								"heroImg": "src/data/course-assets/temp/banner.png"
								
							}
						},
						{
							"Section": {
								"threshold": true,
								"title": "Assessment",
								"children":[
									{
										"Assessment":{
											"scored": true, 
											"masteryScore": 80,
											"settings": {
												"show_feedback": false,
												"review_before_submit": true
											},
											"intro": "Welcome to [ Sample Assessment ]. To get started, click Begin.",
											"PostAssessment":{
												"media": {
													"type": "image",
													"url": "src/data/course-assets/caddy_2.png"
												},
												"text": "Assessment Complete!"
											},
											"questionPools":[
												{
													"pull": 3,
													"title": "Pool 1",
													"questions":[
														
														{
															"type": "mc",
															"directions": "Select the best answer from the choices provided.",
															"questionText": "Sample Question Pool 1, Question A. Which Selection is correct?",
															"reviewContent": [ 0, 2 ],
															"distractors": [
																{
																	"value": "Correct.",
																	"correct": true
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																}
															],
															 "feedback": {
														        "correct": {
														            "value": "That's correct!"
														        },
														        "incorrect": {
														            "value": "That's incorrect."
														        },
														        "tryagain": {
														            "value": "Not quite. Try again."
														        }
														    }
														},
														{
															"type": "mc",
															"directions": "Select the best answer from the choices provided.",
															"questionText": "Sample Question Pool 1, Question B. Which Selection is correct?",
															"reviewContent": [ 0, 2 ],
															"distractors": [
																{
																	"value": "Correct.",
																	"correct": true
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																}
															],
															 "feedback": {
														        "correct": {
														            "value": "That's correct!"
														        },
														        "incorrect": {
														            "value": "That's incorrect."
														        },
														        "tryagain": {
														            "value": "Not quite. Try again."
														        }
														    }
														},
														{
															"type": "mc",
															"directions": "Select the best answer from the choices provided.",
															"questionText": "Sample Question Pool 1, Question C. Which Selection is correct?",
															"reviewContent": [ 0, 2 ],
															"distractors": [
																{
																	"value": "Correct.",
																	"correct": true
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																}
															],
															 "feedback": {
														        "correct": {
														            "value": "That's correct!"
														        },
														        "incorrect": {
														            "value": "That's incorrect."
														        },
														        "tryagain": {
														            "value": "Not quite. Try again."
														        }
														    }
														},
														{
															"type": "mc",
															"directions": "Select the best answer from the choices provided.",
															"questionText": "Sample Question Pool 1, Question D. Which Selection is correct?",
															"reviewContent": [ 0, 2 ],
															"distractors": [
																{
																	"value": "Correct.",
																	"correct": true
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																}
															],
															 "feedback": {
														        "correct": {
														            "value": "That's correct!"
														        },
														        "incorrect": {
														            "value": "That's incorrect."
														        },
														        "tryagain": {
														            "value": "Not quite. Try again."
														        }
														    }
														}
														
													]
												},
												{
													"pull": 2,
													"title": "Pool 2",
													"questions":[
														{
															"type": "mc",
															"directions": "Select the best answer from the choices provided.",
															"questionText": "Sample Question Pool 2, Question A. Which Selection is correct?",
															"reviewContent": [ 0, 2 ],
															"distractors": [
																{
																	"value": "Correct.",
																	"correct": true
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																}
															],
															 "feedback": {
														        "correct": {
														            "value": "That's correct!"
														        },
														        "incorrect": {
														            "value": "That's incorrect."
														        },
														        "tryagain": {
														            "value": "Not quite. Try again."
														        }
														    }
														},
														{
															"type": "mc",
															"directions": "Select the best answer from the choices provided.",
															"questionText": "Sample Question Pool 2, Question B. Which Selection is correct?",
															"reviewContent": [ 0, 2 ],
															"distractors": [
																{
																	"value": "Correct.",
																	"correct": true
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																}
															],
															 "feedback": {
														        "correct": {
														            "value": "That's correct!"
														        },
														        "incorrect": {
														            "value": "That's incorrect."
														        },
														        "tryagain": {
														            "value": "Not quite. Try again."
														        }
														    }
														},
														{
															"type": "mc",
															"directions": "Select the best answer from the choices provided.",
															"questionText": "Sample Question Pool 2, Question C. Which Selection is correct?",
															"reviewContent": [ 0, 2 ],
															"distractors": [
																{
																	"value": "Correct.",
																	"correct": true
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																}
															],
															 "feedback": {
														        "correct": {
														            "value": "That's correct!"
														        },
														        "incorrect": {
														            "value": "That's incorrect."
														        },
														        "tryagain": {
														            "value": "Not quite. Try again."
														        }
														    }
														},
														{
															"type": "mc",
															"directions": "Select the best answer from the choices provided.",
															"questionText": "Sample Question Pool 2, Question D. Which Selection is correct?",
															"reviewContent": [ 0, 2 ],
															"distractors": [
																{
																	"value": "Correct.",
																	"correct": true
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																},
																{
																	"value": "Incorrect",
																	"correct": false
																}
															],
															 "feedback": {
														        "correct": {
														            "value": "That's correct!"
														        },
														        "incorrect": {
														            "value": "That's incorrect."
														        },
														        "tryagain": {
														            "value": "Not quite. Try again."
														        }
														    }
														}
													]
												}
											]
										}
									}
								]
							}
						},
						{
							"Footer": {
								"threshold": true,
								"title": "Footer",
								"directions": "From here you can restart this chapter or continue to the next chapter."
							}
						}
					]
				}
			]
		}