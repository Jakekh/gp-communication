import React from 'react'
import firebase from 'firebase'
import ReactDOM from 'react-dom'
import { Router, Route, IndexRoute, hashHistory, browserHistory } from 'react-router'

import Layout from './main/Layout'
import Course from './main/Course'
import injectTapEventPlugin from 'react-tap-event-plugin'


require("../../node_modules/material-design-lite/material.min.css")
require("../../node_modules/video.js/dist/video-js.css")
require("../../src/css/style.css")

const app = document.getElementById('app')

injectTapEventPlugin()

ReactDOM.render(
	<Layout>
		<Course />
	</Layout>
, app)