import { EventEmitter } from "events"

// ModalStore.createModal({title: "", content: [])// -- use
class ModalStore extends EventEmitter {
	constructor() {
		super()
		this.content = { title: "", content: [] }
	}

	createModal(content, style = null, bumper) {
		this.content = content
		this.style = style
		this.bumper = bumper
		this.emit("modal-opened")
	}
}

const modalStore = new ModalStore

export default modalStore