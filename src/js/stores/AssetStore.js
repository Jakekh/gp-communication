import { EventEmitter } from "events"

import dispatcher from "../dispatcher"

import CourseStore from './CourseStore'
import LmsStore from './LmsStore'

class AssetStore extends EventEmitter {
	constructor() {
		super()
		this.assets = []
	}

	getAssets = (callback) => {
		const courseData = CourseStore.returnCourseData()
		let tmpAry = []
		const progress = LmsStore.progress
		_.map(courseData.modules, (module, modKey) => {
			const moduleProgress = progress[modKey]
			if(module.lessons){
				_.map(module.lessons, (lesson, lesKey) => {
					const lessonProgress = moduleProgress[lesKey]
					let sectionProgress = []
					_.map(lessonProgress, (threshold, thresholdKey) => {
						sectionProgress = sectionProgress.concat(threshold)
					})
					_.map(lesson.sections, (section, secKey) => {
						_.map(section, (item, itemkey) => {
							if(item.meta != undefined) {
								item.meta.sectionId = `${modKey}_${lesKey}_${secKey}`
								item.meta.status = sectionProgress[secKey] == 1 ? 'complete' : 'incomplete'
								tmpAry.push(item)
							}
						})
					})
				})
			}else{
				_.map(module.sections, (section, secKey) => {
					_.map(section, (item, itemkey) => {
					 	item.meta != undefined && tmpAry.push(item)
					})
				})
			}
		})
		this.assets = tmpAry
		callback && callback(this.assets)
	}

	createResultsModal = () => {
		this.emit("show-results")
	}

	handleActions(action) {
		switch(action.type) {
			case "STOP_SEARCH" : {
				this.emit("stop-search")
				break
			}
		}
	}
}

const assetStore = new AssetStore

dispatcher.register(assetStore.handleActions.bind(assetStore))

export default assetStore