var React = require('react')
var $ = require('jquery-easing')

import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import ReactHtmlParser from "react-html-parser"

import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Card, { CardHeader, CardMedia, CardContent, CardActions } from 'material-ui/Card';
import Icon from 'material-ui/Icon'

const styles = theme => ({
  card: {
    maxWidth: 500,
  },
  media: {
    height: 194,
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: "#f00",
  },
});

class Help extends React.Component {
	constructor(){
		super()
		this.state = {
			helpData: require('../../../data/help.json'),
			open: false
		}
	}

	render() {
		
		const { classes } = this.props;
		const content = _.map(this.state.helpData, (value, key) => {
			return (
				<div class={`mdl-cell mdl-cell--${value.cols}-col`}>
					<Card className={classes.card}>
						<CardHeader
	            avatar={
	              <Icon className="material-icons">{ value.icon }</Icon>
	            }
	            title={ value.title }
	          />
		        <CardContent>
		          <Typography className={classes.title}>{ ReactHtmlParser(value.text) }</Typography>
		        </CardContent>
		      </Card>
				</div>
			)
		})
		return <div class="mdl-grid" key="help">{ content }</div>	
	}
}

Help.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Help);
