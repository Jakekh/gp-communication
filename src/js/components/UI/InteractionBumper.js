var React = require('react')

import ReactHtmlParser from "react-html-parser"

import Icon from 'material-ui/Icon'

export default class InteractionBumper extends React.Component {

	render() {
		const { style, text, icon } = this.props
		const styles = {
			centeredContent: {
				width: "100%",
				textAlign: "center",
				padding: "30px 15px",
				zIndex: 9999,
			},
			dirIcon: {
				color: "#fff",
			},
			text: {

			}
		}
		return (
			<div class="continueDirections" style={ style ? { ...style, ...styles.centeredContent } : styles.centeredContent }>
				{ icon && <span><br/><Icon className="material-icons" style={ styles.dirIcon }>error_outline</Icon></span> }
				<span style={styles.text}>{ ReactHtmlParser(text) }</span>
			</div>
			)	
	}
}
