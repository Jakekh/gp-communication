var React = require('react')

export default class SectionDivider extends React.Component {
	
	render() {
		const styles = {
			divider: {
				padding: "50px 0px",
			},
			hr: {
				margin: 0,
			},
			container: {
				background: "#fff",
			}
			
		}

		return (
			<div style={styles.container}>
				<div class="container">
					<div style={styles.divider}>
						<hr style={styles.hr}/>
					</div>
				</div>
			</div>
		)	
	}
}
