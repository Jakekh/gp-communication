var React = require('react')
import ReactHtmlParser from "react-html-parser"

import Button from 'material-ui/Button'

import * as CourseActions from '../../actions/CourseActions'

export default class SpeechBubble extends React.Component {
	constructor (props) {
		super(props)
		this.state = {
		}
	}

	navigate = (location) => {
		CourseActions.loadModule({ module: location[0], lesson: location[1] })
	}

	showFeedback = (fb) => {
		this.setState({ feedback: fb })
	}

	render() {
		const { text, direction, icon, navigation, distractors } = this.props
		const { feedback } = this.state
		
		const actions = _.map(navigation, (value, key) => {
			return <Button key={ `nav-btn-${key}` } class="nav-action" style={{margin: 8, width: '100%'}} variant={ "raised" } color="primary" onTouchTap={ this.navigate.bind(this, value.location) }>{ value.name }</Button>
		})

		const buttons = _.map(distractors, (value, key) => {
			return <Button key={ `dis-btn-${key}` } class="nav-action" style={{margin: 8, width: '100%'}} variant={ "raised" } color="primary" onTouchTap={ this.showFeedback.bind(this, value.feedback) }>{ value.name }</Button>
		})

		return (
			<div class={`speech-bubble speech-bubble-${direction}`}>
				<div class='caret'></div>
				{ icon ? <div class='speechIcon'><i class='material-icons'>{ icon }</i></div> : null }
				{ ReactHtmlParser(text) }
				<div class='caret-cover'></div>
				{ navigation != undefined && <div class="nav-actions-container" style={ {width: '100%', display: 'flex'} }>{ actions }</div> }
				{ distractors != undefined && <div class="nav-actions-container" style={ {width: '100%', display: 'flex'} }>{ buttons }</div> }
				{ feedback != undefined && <div style={ {margin: '16px'} }><hr />{ ReactHtmlParser(feedback) }</div> }
			</div>
		)	
	}
}
