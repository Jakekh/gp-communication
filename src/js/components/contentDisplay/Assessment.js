var React = require('react')
var $ = require('jquery')

import Button from 'material-ui/Button'
import Icon from 'material-ui/Icon'
import IconButton from 'material-ui/IconButton'
import Paper from 'material-ui/Paper'
import ReactHtmlParser from "react-html-parser"
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
import InteractionBumper from '../UI/InteractionBumper'
import AssessmentReview from '../contentDisplay/AssessmentReview'
import update from 'immutability-helper'
import Question from '../contentDisplay/Question'
import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'
import CourseStore from '../../stores/CourseStore'
import MsgStore from '../../stores/MsgStore'
import AssessmentResults from "../contentDisplay/AssessmentResults"
import Divider from 'material-ui/Divider'
import Chip from 'material-ui/Chip'

import { withTheme } from 'material-ui/styles';

class Assessment extends React.Component {
	constructor(props){
		super(props)
		const savedState = LmsStore.returnInteractionData(props.componentId)
		console.log(savedState)
		if(savedState != undefined){
			this.state = {
				score: savedState.score,
				postAssessment: props.PostAssessment,
				questions: savedState.questions,
				currentQuestion: savedState.currentQuestion,
				userData : savedState.userData,
				assessmentComplete: savedState.assessmentComplete,
				scorm_interaction: savedState.scorm_interaction,
				assessmentStarted: savedState.assessmentStarted,
				startTime: savedState.startTime,
				settings: props.settings,
				assessmentReview: savedState.assessmentReview,
				scormInts: savedState.scormInts,
				selection: savedState.selection,
				activeQuestion: savedState.activeQuestion,
				questionSelections: savedState.questionSelections,
				tries: savedState.tries,
			}
		}else{
			this.state = {
				postAssessment: props.PostAssessment,
				questions: null,
				currentQuestion: 0,
				userData : [],
				scorm_interaction: [],
				assessmentStarted: false,
				startTime: null,
				settings: props.settings,
				assessmentReview: false,
				scormInts: [],
				selection: null,
				activeQuestion: null,
				questionSelections: [],
				score: null,
				tries: null,
			}
		}
	}

	componentDidMount() {
		CourseStore.on('questions_generated', this.setQuestions)
		!this.state.questions ?
			CourseStore.generateQuestions(this.props.questionPools)
		: null
	}

	componentWillUnmount() {
		CourseStore.removeListener('questions_generated', this.setQuestions)
	}
	exit = () => {
		MsgStore.createMsg({title: "Exit", message: "Are you sure you want to exit the course?", type: 'prompt', submit: LmsStore.saveSuspendData.bind(this, 'exit')})
	}
	setQuestions = () => {
		this.setState({
			questions: CourseStore.returnQuestions()
		})
	}

	answerQuestion(correctness, selection, answeredText, question){
		let that = this
		const { questions, currentQuestion, userData, scormInts, questionSelections } = this.state

		let userDataUpdate = update(userData, { [currentQuestion] : {$set:  { correctness: correctness, selection: selection } } })

		let correctDistractors = _.find(questions[currentQuestion].distractors, function(d){ return d.correct})
		let latency = new Date().getTime() - this.state.startTime
		let scormObj = {
			weight: 1,
			id: "assessment_"+this.props.componentId+"_"+currentQuestion,
			type: "fill-in",
			objId: 0, //Not dealing with objectives right now.
			timestamp: new Date().toISOString().slice(11, 22),
			correct_responses: _.map(correctDistractors, function(value, key){return value }), 
			learner_response: answeredText,
			result: correctness ? "correct" : "wrong",
			latency: LmsStore.returnFormattedTime(latency),
			description: questions[currentQuestion].questionText
		}
		
		{ !this.props.knowledgeCheck ? LmsStore.saveSCORMinteraction(scormObj) : null}
		

		let questionPos = _.findIndex(scormInts, function(o) { return o.id == "assessment_"+that.props.componentId+"_"+currentQuestion })
		let scormIntsUpdate

		questionPos == "-1" ?
			scormIntsUpdate = update(scormInts, { $push:  [scormObj] })
		:
			scormIntsUpdate = update(scormInts, { [ questionPos ] : {$set:  scormObj} })

		this.setState({
			startTime: new Date().getTime(),
			userData: userDataUpdate,
			correctness: correctness,
			feedback: correctness ? questions[currentQuestion].feedback.correct.value : questions[currentQuestion].feedback.incorrect.value,
			scormInts: scormIntsUpdate,
			tries: questions[currentQuestion].tries ? questions[currentQuestion].tries-1 : null,
		}, function(){
			this.saveInteraction()
		})
	}
	saveInteraction(){
		CourseActions.saveInteraction({
			id: this.props.componentId,
			score: this.state.score, 
			questions: this.state.questions,
			currentQuestion: this.state.currentQuestion,
			userData : this.state.userData,
			scorm_interaction: this.state.scorm_interaction,
			assessmentStarted: this.state.assessmentStarted,
			assessmentComplete: this.state.assessmentComplete,
			startTime: this.state.startTime,
			assessmentReview: this.state.assessmentReview,
			scormInts: this.state.scormInts,
			selection: this.state.selection,
			activeQuestion: this.state.activeQuestion,
			questionSelections: this.state.questionSelections
		})
	}
	reviewQuestion = (data, currentQuestion) => {
		console.log(data)
		const { questions, userData } = this.state
		console.log(currentQuestion)
		this.setState({
			activeQuestion: true,
			currentQuestion: currentQuestion,
			selection: userData[currentQuestion].selection,
		})
	}
	nextQuestion = () => {
		const { questions, currentQuestion, activeQuestion } = this.state

		if(activeQuestion){
			this.setState({
				activeQuestion: false,
				selection: null,
				feedback: null,
				correctness: null,
				tries: questions[currentQuestion].tries ? questions[currentQuestion].tries : null,
			})
		}
		if(questions.length - 1 > currentQuestion){
			this.setState({
				currentQuestion: currentQuestion + 1,
				selection: null,
				feedback: null,
				correctness: null,
				tries: questions[currentQuestion].tries ? questions[currentQuestion].tries : null,
			}, function(){
				this.saveInteraction()
			})
		}else{
			if(this.state.settings.review_before_submit){
				this.setState({
					assessmentReview: true,
					selection: null,
					feedback: null,
					correctness: null,
					tries: null,
				}, function(){
					this.saveInteraction()
				})
			}else{
				this.completeAssessment()
			}
		}
	}
	beginAssessment = () => {// add auto scroll
		this.setState({
			assessmentStarted: true,
			startTime: new Date().getTime(),
		}, function(){
			this.saveInteraction()
		})
		$("html, body").off().stop().animate({ scrollTop: $(`#${this.props.componentId}`).offset().top - 48}, 500, "swing")
	}
	completeAssessment = () => {
		this.setState({
			assessmentReview: false,
			activeQuestion: false,
			selection: null,
			feedback: null,
			assessmentComplete: true,
			tries: null,
		}, function(){
			this.processAssessment()
		})
	}
	retry = () => {
		this.setState({
			score: null,
			currentQuestion: 0,
			userData : [],
			assessmentStarted: false,
			startTime: null,
			assessmentComplete: false,
			tries: null,
		}, function(){
			this.saveInteraction()
		})
	}
	processAssessment = () => {
		const { userData, questions } = this.state
		let numCorrect = 0

		_.map(userData, function(item, key){
			if(item.correctness){
				numCorrect++
				console.log(numCorrect)
			}
		})

		let score =  numCorrect / questions.length * 100
		LmsStore.setScore(score)
		if(score >= this.props.masteryScore){
			this.props.getNextThreshold()
		}
		this.setState({
			score: score
		}, function(){
			this.saveInteraction()
		})
	}
	getMissedQuestions = () => {		
		const { userData, score, questions, assessmentComplete } = this.state

		if(assessmentComplete && score < 100){

			let missedQuestions = []
			let courseData = CourseStore.returnCourseData()
			_.map(questions, function(val, key){
				console.log("Question Missed? ", userData[key] )
				if(userData[key].correctness != true){
					missedQuestions.push(val)
				}
			})

			let grouped = _.groupBy(missedQuestions, 'reviewContent')
			console.log(courseData.modules)

			let reviewList = _.map(grouped, function(section, key){
				let modSecKeys = key.split(',')

				let secQuestions = _.map(section, function(question, key){
					return <div key={`qText_${key}`}><p style={{padding: "0 10px", margin: "10px 0"}}><b>Question:</b> {question.questionText} </p><Divider /></div>
				})

				let questionReviewText
				courseData.modules[parseInt(modSecKeys[0])] ?
					questionReviewText =  <div>
							<h5>{ `${ courseData.modules[parseInt(modSecKeys[0])].title } ${courseData.modules[parseInt(modSecKeys[0])].sections[parseInt(modSecKeys[1])].Section.title}`  }</h5>
							{ secQuestions }
						</div>
				:
				questionReviewText = <p>No Review content Found</p>
				return questionReviewText
			})
			CourseActions.saveAssessmentRem(reviewList)
			return reviewList
		}
	}
	render() {
		const { threshold, title, componentId, intro, knowledgeCheck } = this.props
		const { tries, activeQuestion, userData, scormInts, score, settings, questions, assessmentStarted, assessmentReview, currentQuestion,  feedback, correctness, parMeter, assessmentComplete, postAssessment, completeSectionDirections } = this.state
		
		console.log("Assessment Loading: ", settings)
		let that = this
		const styles = {
			fullWidth: {
				width: "100%",
			},
			assessmentHolder: {
				minHeight: "calc( 100vh - 120px )",
				padding: "40px 0",
			},
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			button: {
				margin: "20px 10px",
			},			
			defBox: {
				position: "relative",
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: "auto",
				background: "#fff",
			},
			closeDefIcon: {
				position: "absolute",
				right: "10px",
				top: "10px",
			},
			defTitle: {
				marginTop: "0px",
			},
			footer: {
				position: "absolute",
				bottom: 0,
				left: 0,
				width: "100%",
				background: "#fff"
			},
			footerText: {
				padding: 20
			},
			chip: {
				backgroundColor: this.props.theme.palette.primary.main,
				color: "#fff",
			},
			headerText: {
				textAlign: "left",
				fontSize: "20px",
				margin: 15
			}
		}

		let questionData = null
		let scoreValues = []
		let scoreValue = []
		let scoreDisplay = null
		let itemStyles = null

		let missedQuestions = settings.review_before_submit ? this.getMissedQuestions() : null
		

		let gridClass = "container mdl-grid"
		!this.props.columnSpacing ? gridClass += " mdl-grid--no-spacing" : null
		if(userData[currentQuestion]){
			console.log(userData[currentQuestion].selection)
		}
		return (
			questions ?
				<div id={ componentId } key={ componentId }>
					<div style={styles.assessmentHolder}>
						<div class={gridClass}>
							{ this.props.title ? 
								<div style={styles.centeredContent}>
									<h2 style={CourseStore.theme.titles}>{this.props.title}</h2>
								</div>
							: null}
							
							{ assessmentStarted && !assessmentComplete && !assessmentReview || activeQuestion || !intro && knowledgeCheck && !assessmentComplete ?
								<div class="mdl-cell mdl-cell--12-col">
									<Chip style={styles.chip} label={`Question ${ currentQuestion + 1 } of ${ questions.length }`} />
									{ questions[currentQuestion].directions ? <h5 style={styles.directions}>{questions[currentQuestion].directions}</h5> : null }
								</div>
							: null }

							{ this.props.headerText && !assessmentComplete ? 
								<div class="mdl-cell mdl-cell--12-col"><div style={styles.headerText}>{this.props.headerText}</div></div>
							: null}

							{ !assessmentStarted && intro ? 
								<div style={styles.centeredContent}>
									<h4>{ intro }</h4>
									<Button color={"primary"} variant={"raised"} onTouchTap={this.beginAssessment}>Begin</Button>
								</div>
							:
								assessmentReview ?
									activeQuestion ?
										<Question 
											feedback={feedback} 
											answerQuestion={this.answerQuestion.bind(this)} 
											questionData={questions[currentQuestion]} 
											settings={settings} 
											correctness={correctness} 
											selection={userData[currentQuestion] ? userData[currentQuestion].selection : null} />
									:
									 <AssessmentReview completeAssessment={this.completeAssessment.bind(this)} scormInts={scormInts} reviewQuestion={this.reviewQuestion.bind(this)} /> 
								: assessmentComplete ?
									<AssessmentResults settings={settings} score={score} postAssessment={postAssessment} retry={this.retry.bind(this)} exit={this.exit.bind(this)} missedQuestions={missedQuestions} />
								:
									<Question 
										knowledgeCheck={knowledgeCheck}
										tries={tries}
										feedbackOpen={feedback ? true : null}
										feedback={feedback} 
										answerQuestion={this.answerQuestion.bind(this)} 
										questionData={questions[currentQuestion]} 
										settings={settings} 
										correctness={correctness} 
										selection={userData[currentQuestion] ? userData[currentQuestion].selection : null} />
									
							}

							{ assessmentStarted && !assessmentComplete && !assessmentReview || activeQuestion ?
								
								<Button disabled={ !feedback && !activeQuestion ? true : false} onTouchTap={this.nextQuestion} color={"primary"} variant={"raised"} > { activeQuestion ? "BACK TO REVIEW" : currentQuestion == questions.length - 1 ? "Results" : "Next Question" } </Button>
							
							: null }
							{ knowledgeCheck && tries === 0 || knowledgeCheck && correctness ?
								
								<Button disabled={ false} onTouchTap={this.nextQuestion} color={"primary"} variant={"raised"} > { "Next Question" } </Button>
							
							: null }
						</div>
					</div>
				</div> 
			: 
				<p>Loading</p>
		)	
	}
}

export default withTheme()(Assessment);