var React = require('react')
var $ = require('jquery')
import _ from 'lodash'
import ReactDOM from 'react-dom'

import Slider from 'react-compound-slider'
import { Handle, Track, Tick } from '../sliderUtils/SliderParts'

import Icon from 'material-ui/Icon'
import IconButton from 'material-ui/IconButton'
import Paper from 'material-ui/Paper'
import ReactHtmlParser from 'react-html-parser'

export default class SlideandDiscover extends React.Component {
	constructor(props){
		super(props)
		// const savedState = LmsStore.returnInteractionData(props.componentId)
		const defaultValues = [1]
		this.state = {
			complete: false,
			completed: Array(props.pairs.length).fill(0),
			values: defaultValues.slice(),
		    update: defaultValues.slice(),
		}
	}

	componentDidMount = () => {
		this.selectItem(this.state.update-1)
	}

	onUpdate = update => {
    	this.setState({ update })
    	this.selectItem(update-1)
  	}

  	onChange = values => {
    	this.setState({ values })
    }

	selectItem = (index) => {
		const that = this
		const item = this.props.pairs[index]
		const outId = this.state.selectedItem ? this.state.selectedItem.item.replace(/\W+/g, '-').toLowerCase() : null
		const defId = item.item.replace(/\W+/g, '-').toLowerCase() 
		$(`#${outId}`).removeClass('expanded').addClass('collapsed')

		let newAry = this.state.completed
		newAry[index] = 1
		
		this.setState({
			selectedItemIndex: index,
			selectedItem: item,
			completed: newAry
		}, function(){
			// CourseActions.saveInteraction({id: that.props.componentId, si: that.state.selectedItemIndex, c: that.state.completed})
			const inId = this.state.selectedItem ? this.state.selectedItem.item.replace(/\W+/g, '-').toLowerCase() : null
			$(`#${inId}`).removeClass('collapsed').addClass('expanded')
			this.checkComplete()
		})
	}

  	checkComplete = () => {
		const { completed } = this.state
		const { pairs } = this.props
		let complete = true
		_.map(pairs, (value, key) => {
			completed[key] == 0 ? complete = false : null
		})
		complete && this.setComplete()
  	}

  	setComplete = () => {
	  	const { getNextThreshold } = this.props
	  	!this.state.complete && getNextThreshold && getNextThreshold()
	  	this.setState({ complete: true })
  	}

	closeDef = () => {
		const that = this
		const defId = this.state.selectedItem.item.replace(/\W+/g, '-').toLowerCase() 
		$(`#${defId}`).removeClass('expanded').addClass('collapsed')
		setTimeout(function(){
			that.setState({
				selectedItem: null
			}, function(){
				// CourseActions.saveInteraction({id: that.props.componentId, si: that.state.selectedItem, c: that.state.completed})
			})
		}, 1000)
	}

	render() {
		const{ title, directions, pairs, componentId, style } = this.props
		const { selectedItem, completed, values, update } = this.state
		const domain = [1, pairs.length]
		const styles = {
			slider: {
				position: 'relative',
				width: '100%',
				marginTop: 10,
				position: 'absolute',
				bottom: 80,
			},
			rail: {
				position: 'absolute',
				width: '100%',
				height: 14,
				borderRadius: 7,
				cursor: 'pointer',
				backgroundColor: '#fff',
			},
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			button: {
				margin: "20px 10px",
			},
			interactionCont:{
				transition: "all 500ms ease-in",
				minHeight: 550,
			},
			shadow: {
			    boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    color: "#898989",
			    background: "rgb(236, 236, 236)",
			},
			inner: {
				minWidth: "100%",
			    color: "#898989",
		        margin: '0 auto',
			    boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    background: "rgb(236, 236, 236)",
			    marginTop: 60,
			},
			interactiveIcon: {
				fontSize: "80px",
				color: "#898989",
			},
			defBoxContainer:{
				display: 'flex',
			},
			defBox: {
				position: "relative",
				width: '100%',
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: '20px auto',
				background: "#fff",
				border: '1px solid #002469',
				display: "inline-block",
				background: '#fff',
			},
			closeDefIcon: {
				position: "absolute",
				right: "10px",
				top: "10px",
			},
			defTitle: {
				marginTop: "0px",
				color: '#002469',
			},
			root: {
				display: 'inline-block',
			},
			gridList: {
				display: 'flex',
				flexWrap: 'nowrap',
				overflowX: 'auto',
			},
			titleStyle: {
				color: 'rgb(0, 188, 212)',
			},
			respImg: {
				cursor: "pointer",
				maxWidth: "100%",
				width: "auto",
			},
			icon: {
				color: "#fff",
			},
			title: {
				fontWeight: "bold",
				textShadow: "none",
			},
		}

		const that = this
		let slider = 
			<div class="slider" style={{ height: 60, width: '100%' }}>
	        	<Slider mode={ 1 } step={ 1 } domain={ domain } rootStyle={ styles.slider } onUpdate={ this.onUpdate } onChange={ this.onChange } values={ values }>
		          	<Slider.Rail>
		            	{({ getRailProps }) => (
		              		<div style={ styles.rail } { ...getRailProps() } />
		            	)}
		          	</Slider.Rail>
		          	<Slider.Handles>
		            	{({ handles, getHandleProps }) => (
		              		<div className="slider-handles">
		                		{handles.map(handle => (
		                  			<Handle key={ handle.id } handle={ handle } domain={ domain } getHandleProps={ getHandleProps } />
		                		))}
		              		</div>
		            	)}
		          	</Slider.Handles>
		         	<Slider.Tracks right={ false }>
		            	{({ tracks, getTrackProps }) => (
		              		<div className="slider-tracks">
		                		{tracks.map(({ id, source, target }) => (
		                  			<Track key={ id } source={ source } target={ target } getTrackProps={ getTrackProps } />
		                		))}
		              		</div>
		            	)}
		          	</Slider.Tracks>
		          	<Slider.Ticks count={ pairs.length }>
			            {({ ticks }) => (
			              	<div className="slider-ticks">
			                	{ticks.map((tick, key) => (
			                  		<Tick key={ tick.id } tick={ tick } count={ ticks.length } name={ pairs[key].item } />
			                	))}
			              	</div>
			            )}
			        </Slider.Ticks>
		        </Slider>
	      	</div>

		const defId = selectedItem ? selectedItem.item.replace(/\W+/g, '-').toLowerCase() : null
		const defBox = selectedItem ? <div style={ styles.defBoxContainer } id={ defId }>
										<Paper style={ styles.defBox } key={selectedItem.item}>
											<h3 style={ styles.defTitle }>{ selectedItem.item }</h3>
											<div class='defBoxContent' style={styles.defBoxContent}>
												{ ReactHtmlParser(selectedItem.def) }
											</div>
										</Paper>
									</div> : null 

		let gridClass = "container mdl-grid"
		!this.props.columnSpacing ? gridClass += " mdl-grid--no-spacing" : null

		return (
			<div style={ title ? this.props.innerChild ? { ...styles.inner, ...style } : { ...styles.shadow, ...style } : style }>
				<div style={ styles.interactionCont } class={ gridClass } key={ componentId }>
					<div style={ styles.centeredContent }>
						{ title ? <h2 >{ title }</h2> : null }
						{ directions ? <h4 >{ directions }</h4> : null } 
						<div>
							{ defBox }
							<div class="slider-container">{ slider }</div>
						</div>
					</div>
				</div>
			</div>
		)	
	}
}
