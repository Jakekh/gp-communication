import firebase from 'firebase'
import _ from 'lodash'

import dispatcher from '../dispatcher'

export function stopSearch() {
	dispatcher.dispatch({
		type: 'STOP_SEARCH'
	})
}

export function sendResults(results) {
	dispatcher.dispatch({
		type: 'SEND_RESULTS',
		results: results
	})
}