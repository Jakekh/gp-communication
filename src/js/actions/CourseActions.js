import firebase from 'firebase'
import _ from 'lodash'

import dispatcher from '../dispatcher'

export function getCourseData(data) {
	dispatcher.dispatch({
		type: 'GETTING_COURSE',
		course: data
	})
}
export function createView(location) {
	dispatcher.dispatch({
		type: 'GETTING_VIEW',
		location: location
	})
}
export function loadModule(data) {
	dispatcher.dispatch({
		type: 'LOAD_NEW_MODULE',
		module: data.module,
		lesson: data.lesson
	})
}
export function loadNextThreshold() {
	dispatcher.dispatch({
		type: 'NEXT_THRESHOLD'
	})
}
export function saveInteraction(interactionObj) {
	dispatcher.dispatch({
		type: 'SAVE_INTERACTION_STATE',
		obj: interactionObj
	})
}

export function infolink(term) {
	dispatcher.dispatch({
		type: 'SHOW_GLOSSARY_TERM',
		term: term
	})
}

export function saveAssessmentRem(remed) {
	dispatcher.dispatch({
		type: 'SAVE_ASSESSMENT_REM',
		rem: remed
	})
}
export function updateProgress(loc) {
	//console.log("PROGRESS: ", loc)
	dispatcher.dispatch({
		type: 'UPDATE_PROGRESS',
		location: loc
	})
}

